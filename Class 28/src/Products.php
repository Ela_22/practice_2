<?php

class Products 
{
    public $conn;
    public function __construct()
    {
        try
        {
            session_start();
            $this->conn = new PDO("mysql:host=localhost; dbname=ela", "root", "root");
        }
        catch(PDOException $exception)
        {
            session_start();
            $_SESSION['error'] = "Failed to connect! ERROR :  " . $exception->getMessage();
            header("location: ../index.php");
        }
    }

    
    
    //SHOW
    public function show_phone()
    {
        $query = "select * from phone";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll();
        return $data;
    }

    public function show_laptop()
    {
        $query = "select * from laptop";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll();
        return $data;
    }

    public function show_car()
    {
        $query = "select * from car";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll();
        return $data;
    }

    public function show_bike()
    {
        $query = "select * from bike";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll();
        return $data;
    }




    //STORE
    public function store_in_phone($data)
    {
        $id = $data['id'];
        $name = $data['name'];
        $cost = $data['cost'];
        try
        {
        $query = "insert into phone(product_id, name, cost) values (:phone_id, :phone_name, :phone_cost)";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'phone_id' => $id,
            'phone_name' => $name,
            'phone_cost' => $cost
        ]);

        $_SESSION['message'] = "Data stored successfully!";
        header("location: phone.php");
        }
        catch(PDOException $exception)
        {   
            session_start();
            $_SESSION['error'] = "Data submission failed, ERROR:  " . $exception->getMessage();
            header("location: phone-form.php");
        }
    }


    public function store_in_laptop($data)
    {
        $id = $data['id'];
        $name = $data['name'];
        $cost = $data['cost'];
        try
        {
        $query = "insert into laptop(product_id, name, cost) values (:phone_id, :phone_name, :phone_cost)";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'phone_id' => $id,
            'phone_name' => $name,
            'phone_cost' => $cost
        ]);

        $_SESSION['message'] = "Data stored successfully!";
        header("location: laptop.php");
        }
        catch(PDOException $exception)
        {   
            session_start();
            $_SESSION['error'] = "Data submission failed, ERROR:  " . $exception->getMessage();
            header("location: laptop-form.php");
        }
    }


    public function store_in_car($data)
    {
        $id = $data['id'];
        $name = $data['name'];
        $cost = $data['cost'];
        try
        {
        $query = "insert into car(product_id, name, cost) values (:phone_id, :phone_name, :phone_cost)";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'phone_id' => $id,
            'phone_name' => $name,
            'phone_cost' => $cost
        ]);

        $_SESSION['message'] = "Data stored successfully!";
        header("location: car.php");
        }
        catch(PDOException $exception)
        {   
            session_start();
            $_SESSION['error'] = "Data submission failed, ERROR:  " . $exception->getMessage();
            header("location: car-form.php");
        }
    }


    public function store_in_bike($data)
    {
        $id = $data['id'];
        $name = $data['name'];
        $cost = $data['cost'];
        try
        {
        $query = "insert into bike(product_id, name, cost) values (:phone_id, :phone_name, :phone_cost)";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'phone_id' => $id,
            'phone_name' => $name,
            'phone_cost' => $cost
        ]);

        $_SESSION['message'] = "Data stored successfully!";
        header("location: bike.php");
        }
        catch(PDOException $exception)
        {   
            session_start();
            $_SESSION['error'] = "Data submission failed, ERROR:  " . $exception->getMessage();
            header("location: bike-form.php");
        }
    }




    public function update_phone($data)
    {
        $id = $data['id'];
        $product_id = $data['product_id'];
        $name = $data['name'];
        $cost = $data['cost'];
        $query = "update phone set product_id = :phone_id, name = :phone_name, cost = :phone_cost where id = $id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'phone_id' => $product_id,
            'phone_name' => $name,
            'phone_cost' => $cost
        ]);

        $_SESSION['message'] = "Successfully Updated!";
        header("location: phone.php");
    }

    public function update_laptop($data)
    {
        $id = $data['id'];
        $product_id = $data['product_id'];
        $name = $data['name'];
        $cost = $data['cost'];
        $query = "update laptop set product_id = :laptop_id, name = :laptop_name, cost = :laptop_cost where id = $id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'laptop_id' => $product_id,
            'laptop_name' => $name,
            'laptop_cost' => $cost
        ]);

        $_SESSION['message'] = "Successfully Updated!";
        header("location: laptop.php");
    }

    public function update_car($data)
    {
        $id = $data['id'];
        $product_id = $data['product_id'];
        $name = $data['name'];
        $cost = $data['cost'];
        $query = "update car set product_id = :car_id, name = :car_name, cost = :car_cost where id = $id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'car_id' => $product_id,
            'car_name' => $name,
            'car_cost' => $cost
        ]);

        $_SESSION['message'] = "Successfully Updated!";
        header("location: car.php");
    }


    public function update_bike($data)
    {
        $id = $data['id'];
        $product_id = $data['product_id'];
        $name = $data['name'];
        $cost = $data['cost'];
        $query = "update bike set product_id = :phone_id, name = :phone_name, cost = :phone_cost where id = $id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'bike_id' => $product_id,
            'bike_name' => $name,
            'bike_cost' => $cost
        ]);

        $_SESSION['message'] = "Successfully Updated!";
        header("location: bike.php");
    }

    // Delete
    public function delete_phone($id)
    {
        try
        {
            $query ="delete from phone where id =:phone_id";
            $stmt =$this->conn->prepare($query);
            $stmt->execute([
                'phone_id'=>$id
            
            ]);
        }
        catch(PDOException $exception)
        {
            $_SESSION['Error']=$exception->getMessage();
            header(("location:phone.php"));
        }
    }

    public function delete_laptop($id)
    {
        try
        {
            $query ="delete from laptop where id =:laptop_id";
            $stmt =$this->conn->prepare($query);
            $stmt->execute([
                'laptop_id'=>$id
            
            ]);
        }
        catch(PDOException $exception)
        {
            $_SESSION['Error']=$exception->getMessage();
            header(("location:laptop.php"));
        }
        
    }


    public function delete_car($id)
    {
        try
        {
            $query ="delete from car where id =:car_id";
            $stmt =$this->conn->prepare($query);
            $stmt->execute([
                'car_id'=>$id
            
            ]);
        }
        catch(PDOException $exception)
        {
            $_SESSION['Error']=$exception->getMessage();
            header(("location:car.php"));
        }
        
    }

    public function delete_bike($id)
    {
        try
        {
            $query ="delete from bike where id =:bike_id";
            $stmt =$this->conn->prepare($query);
            $stmt->execute([
                'bike_id'=>$id
            
            ]);
        }
        catch(PDOException $exception)
        {
            $_SESSION['Error']=$exception->getMessage();
            header(("location:bike.php"));
        }
        
    }

}
