<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Phone</title>
    <style>
        body{
            text-align: center;
        }
        table{
            text-align: center;
            width: 100%;
        }
    </style>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="../index.php">Home</a>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                PRODUCTS
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="phone.php">Phone</a></li>
                <li><a class="dropdown-item" href="laptop.php">Laptop</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="car.php">Car</a></li>
                <li><a class="dropdown-item" href="bike.php">Bike</a></li>
            </ul>
            </li>
        </ul>
        <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
        </div>
    </div>
    </nav>



    <?php 
        include '../src/Products.php';
        $obj = (new Products())->show_bike();
        
    ?>
    <h2>List of Bikes</h2>
    <a href="bike-form.php">Insert Data</a><br>
    <table class="table table-dark table-hover">
  <thead>
    <tr>
      <th scope="col">SL</th>
      <th scope="col">Name</th>
      <th scope="col">Cost Price</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php 
        $sl = 1;
        foreach($obj as $sale){ 
    ?>
    <tr>
        <th scope="row"><?= $sl++ ?></th>
        <td><?= $sale['name'] ?></td>
        <td><?= $sale['cost'] ?></td>
        <td> <a href="bike-edit.php?id=<?=$sale['id']?>">Edit</a></td>
        <td><a href="bike-delete.php?id=<?=$sale['id']?>">Delete</td>
    </tr>
    <?php } ?>
  </tbody>
</table>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
