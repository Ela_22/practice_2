<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Index</title>
    <style>
        body{
            text-align: center;
        }
    </style>
</head>
<body>
<!-- nabbar start -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="index.php">Home</a>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                PRODUCTS
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="views/phone.php">Phone</a></li>
                <li><a class="dropdown-item" href="views/laptop.php">Laptop</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="views/car.php">Car</a></li>
                <li><a class="dropdown-item" href="views/bike.php">Bike</a></li>
            </ul>
            </li>
        </ul>
        <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
        </div>
    </div>
    </nav>
    <!-- nabbar end -->

    <!-- veriabel start -->
    <?php

    include_once 'src/Products.php';
    $object = new Products();
    $showphone =$object->show_phone();
    $showlaptop =$object->show_laptop();
    $showcar =$object->show_car();
    $showbike =$object->show_bike();
    
    // veriabel end

    if(isset($_SESSION['message']))
    {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    } 
    else if(isset($_SESSION['error']))
    {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }
    ?>
    
<!-- header start -->
    <h2>Welcome to The Showroom!</h2>
    <h3>We provide the best quality products found in the market.</h3>

    <!-- header end -->

    <!-- card start -->

    <div class="row row-cols-1 row-cols-md-4 g-4">
  <div class="col">
      <?php foreach($showphone as $v){?>
    <div class="card">
      <img src="..." class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title"><?=$v['name']?></h5>
        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      </div>
    </div>
    <?php echo"<br>"; } ?>
  </div>
  <div class="col">
  <?php foreach($showlaptop as $v){?>
    <div class="card">
      <img src="..." class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title"><?=$v['name']?></h5>
        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      </div>
    </div>
    <?php echo"<br>";} ?>
  </div>
  <div class="col">
    <?php foreach($showcar as $v){?>
    <div class="card">
      <img src="..." class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title"><?=$v['name']?></h5>
        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
      </div>
    </div>
    <?php echo"<br>";} ?>
  </div>
  <div class="col">
  <?php foreach($showbike as $v){?>
    <div class="card">
      <img src="..." class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title"><?=$v['name']?></h5>
        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      </div>
    </div>
    <?php echo"<br>";} ?>
  </div>
  
</div>

<!-- card end -->










    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>