<?php
 class Category
 {
     public $conn ;
     public function __construct()

     {
         try
         {
             $this->conn = new PDO("mysql:host=localhost;dbname=ela","root","root");
             $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
             echo "connection success";
         }
         catch(PDOException $exception)
         {
            session_start();
            $_SESSION['message'] ="connenction Failed ! ERROR : " .$exception->getMessage();
            header("location:../views/create.php");
         }
         
     }
    

     public function store($data)
     {
        print_r($data); 

        $title = $data['title'];
        $color=$data['color'];
        $brand=$data['brand'];
        $description = $data['description'];

        try
        {
            $query ="insert into  categori (title,description,color,brand) values(:category_title, :category_description,:category_color,:category_brand)";
            $stmt =$this ->conn->prepare($query);
            $stmt->execute([
                'category_title' => $title,
                'category_color' => $color,
                'category_brand' => $brand,
                'category_description' => $description,
            ]);
            echo "Data Inserted";
        }
        catch(PDOException $exception)
        {
            session_start();
            $_SESSION['message'] ="Failed submission data, ERROR : " . $exception->getMessage();
            header("location:../views/create.php");
        }

     }
     public function index()
     {
         $query ="select * from categori";
         $stmt =$this->conn->prepare($query);
         $stmt->execute();
         $data =$stmt->fetchAll();
         return $data;
     }
     public function show($id)
     {
         $query ="select * from  categori where id = :category_id";
         $stmt = $this->conn->prepare($query);
         $stmt-> execute([
             'category_id' =>$id
         ]);
         $data =$stmt->fetch();
         return $data;

     }

     public function edit($id)
     {
         $query="select * from categori where id = :category_id";
         $stmt =$this->conn->prepare($query);
         $stmt->execute([
             'category_id' =>$id
         ]);
          return $stmt->fetch();
        

     }

     public function update($data)
     {
        // print_r($data); 

        

        try
        {
            $id=$data['id'];
            $title = $data['title'];
            $color=$data['color'];
            $brand=$data['brand'];
            $description = $data['description'];

            $query ="update categori  set title=:category_title,description=:category_description,color=:category_color,brand=:category_brand where id =:category_id";
            $stmt =$this ->conn->prepare($query);
            $stmt->execute([
                'category_title' => $title,
                'category_color' => $color,
                'category_brand' => $brand,
                'category_description' => $description,
                'category_id' =>$id,
            ]);
            // echo "Data Inserted";
            $_SESSION['message']="Successfully update !";
            header('location:index.php');
        }
        catch(PDOException $exception)
        {
            session_start();
            $_SESSION['message'] ="Failed submission data, ERROR : " . $exception->getMessage();
            header("location:../views/create.php");
        }

     }
     public function destroy($id)

     {
         try
         

         {
         $query ="delete  from  categories where id = :category_id";
         $stmt = $this->conn->prepare($query);
         $stmt-> execute([
             'category_id' =>$id
         ]);

         $_SESSION['error']= 'Successfully Deleted'; 
         header('location:../views/index.php ');
         
        }
        catch (PDOException $exception)
        {
            $_SESSION['error']= $exception->getMessage(); 
            header('location:../views/index.php ');
        }
     }


 }
