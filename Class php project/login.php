<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login page</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
      body{
        background-color: rgb(0, 200, 150);
        color: black;
      }
      label{
        font-weight: bold;
        font-size: 20px;
      }
      input{
        border: 3px solid black;
      }
  </style>
</head>
<body>
    

<div class="container">
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <fieldset>
        <legend style="text-align: center; font-size: 35px; font-weight: bolder;"><em>Log In</em></legend><br><br>
          <form>
              <div class="row">
                <div class="col-md-4" >
                  <label for="email">Email : </label>
                </div>
                <div class="col-md-8">
                  <input class="form-control-lg" type="email" id="email" placeholder="Enter email" name="email">
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-4">
                  <label for="pwd">Password : </label>
                </div>
                <div class="col-md-8">
                  <input class="form-control-lg" type="password" id="pwd" placeholder="Enter password" name="pswd">
                </div>
              </div><br>
              <div>
                <label><input type="checkbox" name="remember"> Remember me </label>
              </div><br>
              <button type="submit" class="btn btn-primary">Log In</button>
          </form>
      </fieldset>
    </div>
    <div class="col-md-3"></div>
  </div>
</div>
<br><br>
    
</body> 

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</html>