<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Index</title>
    <style>
        body{
            text-align: center;
        }
    </style>
</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="index.php">Home</a>
            <a class="nav-link active" aria-current="page" href="login.php">Log In</a>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                PRODUCTS
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="views/phone-list.php">Phone List</a></li>
                <li><a class="dropdown-item" href="views/phone.php">Phone Database</a></li>
                <li><a class="dropdown-item" href="views/laptop-list.php">Laptop List</a></li>
                <li><a class="dropdown-item" href="views/laptop.php">Laptop Database</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="views/car-list.php">Car List</a></li>
                <li><a class="dropdown-item" href="views/car.php">Car Database</a></li>
                <li><a class="dropdown-item" href="views/bike-list.php">Bike List</a></li>
                <li><a class="dropdown-item" href="views/bike.php">Bike Database</a></li>
            </ul>
            </li>
        </ul>
        <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
        </div>
    </div>
    </nav>

    
    

    <h2>Welcome to The Showroom!</h2>
    <h3>We provide the best quality products found in the market.</h3><br><br>

    <?php 
        include 'src/Products.php';
        $obj = new Products();
        $sp = $obj->show_phone();
        $sl = $obj->show_laptop();
        $sc = $obj->show_car();
        $sb = $obj->show_bike();
        
    
    if(isset($_SESSION['message']))
    {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    } 
    else if(isset($_SESSION['error']))
    {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }
    
    ?>



    <div class="row row-cols-1 row-cols-md-4 g-4">
  <div class="col">
      <h2>Phone</h2>
  <?php foreach($sp as $v){ ?>
    <div class="card">
      <img src="imgs/phone.jpg" class="card-img-top" alt="..." height="300px">
      <div class="card-body">
        <h5 class="card-title"><?= $v['name'] ?></h5>
        <p class="card-text">Cost Price : <?= $v['cost'] ?></p>
        <a href="phone-list.php">Details</a>
      </div>
    </div>
    <?php echo "<br>";} ?>
  </div>
  <div class="col">
  <h2>Laptop</h2>
  <?php foreach($sl as $v){ ?>
    <div class="card">
      <img src="imgs/laptop.png" class="card-img-top" alt="..." height="300px">
      <div class="card-body">
      <h5 class="card-title"><?= $v['name'] ?></h5>
      <p class="card-text">Cost Price : <?= $v['cost'] ?></p>
      <a href="laptop-list.php">Details</a>
      </div>
    </div>
    <?php echo "<br>";} ?>
  </div>
  <div class="col">
  <h2>Car</h2>
  <?php foreach($sc as $v){ ?>
    <div class="card">
      <img src="imgs/car.jfif" class="card-img-top" alt="..." height="300px">
      <div class="card-body">
      <h5 class="card-title"><?= $v['name'] ?></h5>
      <p class="card-text">Cost Price : <?= $v['cost'] ?></p>
      <a href="car-list.php">Details</a>
      </div>
    </div>
    <?php echo "<br>";} ?>
  </div>
  <div class="col">
  <h2>Bike</h2>
  <?php foreach($sb as $v){ ?>
    <div class="card">
      <img src="imgs/bike.jfif" class="card-img-top" alt="..." height="300px">
      <div class="card-body">
      <h5 class="card-title"><?= $v['name'] ?></h5>
      <p class="card-text">Cost Price : <?= $v['cost'] ?></p>
      <a href="bike-list.php">Details</a>
      </div>
    </div>
    <?php echo "<br>";} ?>
  </div>
</div>




    

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>