<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update List</title>
</head>
<body>
    <?php
        session_start();
        if(isset($_SESSION['error'])){
            echo $_SESSION['error'];
            unset($_SESSION['error']);
        }
        if(isset($_SESSION['update'])){
            echo $_SESSION['update'];
            unset($_SESSION['update']);
        }
    ?>

    <h2><a href="index.php">Index</a></h2><br><br><br>

    <form action="update.php">
        <input type="hidden" name="id" value="<?= $_GET['id'] ?>" />
        <input type="text" name="name" placeholder="Enter Product Name." />
        <input type="text" name="description" placeholder="Description." />
        <input type="number" name="cost" placeholder="Enter Cost Price."/>
        <input type="number" name="category_id" placeholder="Enter Category ID."/>
        <button type="submit">Submit</button>
    </form>
    
</body>
</html>