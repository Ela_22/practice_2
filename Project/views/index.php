<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>CRUD Project</title>
    <style>
        body{
            text-align: center;
        }
    </style>
</head>
<body>
    <?php 
        include '../src/Products.php';
        $obj = new Products();
        $sc = $obj->show_categories();
        $sp = $obj->show_products();
        
        
    
    if(isset($_SESSION['message']))
    {
      //print_r($_SESSION['message']);
      //unset($_SESSION['message']);
    }else
    {
      $_SESSION['message'] = $sp;
    }

    if(isset($_SESSION['update']))
    {
        echo $_SESSION['update'];
        unset($_SESSION['update']);
    }
    if(isset($_SESSION['error']))
    {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }

    ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item"><a class="nav-link active" aria-current="page" href="index.php">Home</a></li>
            <?php if(isset($_SESSION['isAuthenticated'])) { if($_SESSION['isAuthenticated'] == true) {?>
            <li><a class="nav-link active" aria-current="page" href="insert.php">Insert</a></li>
            <?php }} ?>
            
            
            <?php if(isset($_SESSION['isAuthenticated'])) { if($_SESSION['isAuthenticated'] == true) {?>
              <li><a class="nav-link active" aria-current="page" href="../login/logout.php">Log Out</a></li>
            <?php } else if($_SESSION['isAuthenticated'] == false) {?>
              <li><a class="nav-link active" aria-current="page" href="../login/login.php">Log In</a></li>
            <?php }}else{ ?>
              <li><a class="nav-link active" aria-current="page" href="../login/login.php">Log In</a></li>
            <?php } ?>


            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                PRODUCTS
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <?php foreach($sc as $v){  ?>
                <li><a class="dropdown-item" href="<?php echo $v['name'] ?>.php"><?php echo $v['name'] ?></a></li>
                <?php  } ?>
            </ul>
            </li>
        </ul>
        <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
            <?php if(isset($_SESSION['isAuthenticated'])) { if($_SESSION['isAuthenticated'] == true) {?>
            <button class="btn btn-outline-success" type="button"><a href="../login/dashboard.php">Dashboard</a></button>
            <?php }} ?>
        </form>
        </div>
    </div>
    </nav>

    
    

    <h2>Welcome to The Showroom!</h2>
    <h3>We provide the best quality products found in the market.</h3><br><br>


<div class="row row-cols-1 row-cols-md-4 g-4">
  <?php foreach($_SESSION['message'] as $v){ ?>
    <div class="col">
      <div class="card">
        <img src="../imgs/<?php echo $v['image'] ?>" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title"><?php echo $v['name'] ?></h5>
          <p class="card-text">Cost Price : <?php echo $v['cost'] ?></p>
          <a href="details.php?id=<?php echo $v['id']?>">Details</a>
          <?php if(isset($_SESSION['isAuthenticated'])){ 
          if($_SESSION['isAuthenticated'] == true){ ?>
          <a href="edit.php?id=<?php echo $v['id']?>">Update</a>
          <a href="soft-delete.php?id=<?php echo $v['id']?>">Delete</a>
          <?php }} ?>
        </div>
      </div>    
    </div>
  <?php echo "<br>";} ?>
</div>

       
        

    

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>