<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Log In</title>
</head>
<body>
    
    <h1><a href="../views/index.php">Home</a></h1>

    <?php
        session_start();
        if(isset($_SESSION['errorAuth'])){
            echo $_SESSION['errorAuth'];
            unset($_SESSION['errorAuth']);
        }
    ?>

    <form action="login-process.php" method="post">
        <input name='username' type='text' placeholder='Enter Username' />
        <input name='password' type='password' placeholder='Enter Password' />
        <button type='submit'>Log In</button>
    </form>


</body>
</html>