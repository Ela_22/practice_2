<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Dashboard</title>
</head>
<body>
    <?php
    include_once '../src/Products.php';
    $obj = new Products();
    include_once 'guard.php';

    $sc = $obj->show_categories();
        
        
    
    if(isset($_SESSION['message']))
    { 
      //print_r($_SESSION['message']);
      //unset($_SESSION['message']);
    }else
    {
      $_SESSION['message'] = $sp;
    }

    if(isset($_SESSION['update']))
    {
        echo $_SESSION['update'];
        unset($_SESSION['update']);
    }
    if(isset($_SESSION['error']))
    {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }

    $sp = $_SESSION['message'];
    
    ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item"><a class="nav-link active" aria-current="page" href="../views/">Home</a></li>
            <li><a class="nav-link active" aria-current="page" href="logout.php">Log Out</a></li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                PRODUCTS
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <?php foreach($sc as $v){  ?>
                <li><a class="dropdown-item" href="<?php echo $v['name'] ?>.php"><?php echo $v['name'] ?></a></li>
                <?php  } ?>
            </ul>
            </li>
        </ul>
        <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
        </div>
    </div>
    </nav>




<div class="row">

    <div class="col-md-2">
        <aside>
            <h2><em>Sidebar</em></h2><br>
            <h2><a href="../views/insert.php">Insert</a></h2>
            <h2><a href="trash.php">Recycle Bin</a></h2>
            <div class="dropdown">
            <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            Categories
            </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <?php foreach($sc as $v){  ?>
            <li><a class="dropdown-item" href="<?php echo $v['name'] ?>.php"><?php echo $v['name'] ?></a></li>
            <?php  } ?>
        </ul>        
        </aside>
    </div>



<div class="col-md-10">
<h1 style="text-align: center;">Welcome to the Dashboard <?php echo $_SESSION['username'] ?></h1>
<table class="table table-dark table-hover">
  <thead>
    <tr>
      <th scope="col">SL</th>
      <th scope="col">Name</th>
      <th scope="col">Cost Price</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php 
        $sl = 1;
        foreach($sp as $sale){ 
    ?>
    <tr>
        <th scope="row"><?= $sl++ ?></th>
        <td><?= $sale['name'] ?></td>
        <td><?= $sale['cost'] ?></td>
        <td><a href="../views/edit.php?id= <?= $sale['id'] ?>">Edit</a></td>
        <td><a href="../views/soft-delete.php?id= <?= $sale['id'] ?>" onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
        </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>