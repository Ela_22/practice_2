<?php 

class Products
{
    public $conn;
    public function __construct()
    {
        try
        {
            session_start();
            $this->conn = new PDO("mysql:host=localhost; dbname=abm", "root", "root");
        }
        catch(PDOException $exception)
        {
            session_start();
            $_SESSION['error'] = "Failed to connect! ERROR :  " . $exception->getMessage();
            header("location: index.php");
        }
    }


    public function show_categories()
    {
        $query = "select * from categories";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_ASSOC
        return $data;
    }

    public function show_products()
    {
        $query = "select * from products where is_deleted is null";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_ASSOC
        return $data;
    }

    public function show_phone()
    {
        $query = "select * from products where category_id = 1 and is_deleted is null";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_ASSOC
        return $data;
    }

    public function show_laptop()
    {
        $query = "select * from products where category_id = 2 and is_deleted is null";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_ASSOC
        return $data;
    }

    public function show_car()
    {
        $query = "select * from products where category_id = 3 and is_deleted is null";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_ASSOC
        return $data;
    }

    public function show_bike()
    {
        $query = "select * from products where category_id = 4 and is_deleted is null";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_ASSOC
        return $data;
    }


    public function show_details($id)
    {
        $query = "select * from products where id = $id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_ASSOC
        return $data;
    }

    public function show_recycle_bin()
    {
        $query = "select * from products where is_deleted is not null";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_ASSOC
        return $data;
    }




    public function store($data)
    {
        $category_id = $data['category_id'];
        $name = $data['name'];
        $cost = $data['cost'];
        $description = $data['description'];
        try
        {
        $query = "insert into products(name, description, cost, category_id) values (:product_name, :product_desc, :product_cost, :product_category_id)";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'product_name' => $name,
            'product_desc' => $description,
            'product_cost' => $cost,
            'product_category_id' => $category_id
        ]);

        $_SESSION['update'] = "Data stored successfully!";
        $_SESSION['message'] = (new Products())->show_products();
        header("location: ../login/dashboard.php");
        }
        catch(PDOException $exception)
        {   
            $_SESSION['error'] = "Data submission failed, ERROR:  " . $exception->getMessage();
            header("location: ../login/dashboard.php");
        }
    }



    public function update($data)
    {
        try
        {
            $id = $data['id'];
            $name = $data['name'];
            $description = $data['description'];
            $cost = $data['cost'];
            $category_id = $data['category_id'];

            $query = "update products set name = :update_name, description = :update_desc, cost = :update_cost, category_id = :update_category_id where id = :update_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'update_name' => $name,
                'update_desc' => $description,
                'update_cost' => $cost,
                'update_category_id' => $category_id,
                'update_id' => $id
            ]);

            $_SESSION['update'] = "Successfully Updated!";
            $_SESSION['message'] = (new Products())->show_products();
            header("location: ../login/dashboard.php");
        }
        catch(PDOException $e)
        {
            $_SESSION['error'] = "Data update failed. ERROR : " . $e->getMessage();
            header("location: edit.php");
        }
    }


    public function delete($id)
    {
        try
        {
            $query = "delete from products where id = $id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $_SESSION['update'] = "Successfully Deleted!";
            $_SESSION['message'] = (new Products())->show_products();
            header("location: ../login/dashboard.php");
        }
        catch(PDOException $e)
        {
            $_SESSION['error'] = "Data update failed. ERROR : " . $e->getMessage();
            header("location: ../login/dashboard.php");
        }
    }

    public function soft_delete($id)
    {
        try
        {
            $query = "update products set is_deleted = :value where id = $id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'value' => 1
            ]);

            $_SESSION['update'] = "Successfully Deleted!";
            $_SESSION['message'] = (new Products())->show_products();
            header("location: ../login/dashboard.php");
        }
        catch(PDOException $e)
        {
            $_SESSION['error'] = "Data update failed. ERROR : " . $e->getMessage();
            header("location: ../login/dashboard.php");
        }
    }

    public function restore($id)
    {
        try
        {
            $query = "update products set is_deleted = :value where id = $id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'value' => null
            ]);

            $_SESSION['update'] = "Successfully Deleted!";
            $_SESSION['message'] = (new Products())->show_products();
            header("location: ../login/dashboard.php");
        }
        catch(PDOException $e)
        {
            $_SESSION['error'] = "Data update failed. ERROR : " . $e->getMessage();
            header("location: ../login/dashboard.php");
        }
    }
    
}