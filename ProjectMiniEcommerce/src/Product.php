<?php
include_once 'Connection.php';
class Product extends Connection{

    
    public function store($data){
     
        $name=$data['pname'];
        $description=$data['description'];
        $price=$data['price'];
        $category_id=$data['category_id'];
        $filename=$_FILES['picture']['name'];
        $tempname=$_FILES['picture']['tmp_name'];
        $uniqueimagename=time().$filename;
        move_uploaded_file($tempname,'../../assets/image/'.$uniqueimagename);
        try{
          $query="insert into products(product_name,description,price,categori_id,picture) values('$name','$description','$price','$category_id','$uniqueimagename')";
          $this->conn->exec($query);
        }catch(PDOException $ex){
            echo ($ex->getMessage());
        }
        
      }
      public function view($id=null){
        
         if($id){
         $query="select * from products where categori_id=$id";
         }
         else{
            $query="select * from products order by categori_id desc ";
         }
         
          $stmt=$this->conn->prepare($query);
          $stmt->execute();
          return $stmt->fetchAll();
         
      }
      public function show($id){
        
    
        $query="select * from products where id=$id ";
        
        
         $stmt=$this->conn->prepare($query);
         $stmt->execute();
         return $stmt->fetch();
        
     }
     public function delete($id){
       
      $query="delete from products where id=$id";
      $stmt=$this->conn->prepare($query);
      $stmt->execute();

     }
     public function update($data){
      $name=$data['pname'];
      $description=$data['description'];
      $price=$data['price'];
      $category_id=$data['category_id'];
      $id=$data['id'];
       try {
         $query="update products set product_name=:p_name, description=:des,price=:price,categori_id=:cat_id where id=$id";
         $stmt=$this->conn->prepare($query);
         $stmt->execute(
           [
             'p_name'=>$name,
             'des'=>$description,
             'price'=>$price,
             'cat_id'=>$category_id,
           ]
         );
       } catch (PDOException $ex) {
          echo($ex->getMessage());
       }


     }
      
}