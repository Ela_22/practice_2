<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Product Category</title>
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
</head>
<body>
<script src="../../assets//js/bootstrap.bundle.min.js"></script>
<nav class="navbar navbar-expand-lg nnavbar navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="../index.php">
                <img src="../../assets/image/rupali.jpeg" alt="" width="100" height="60" class="d-inline-block align-text-top">
                রুপালী বাজার
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 " style="margin-left: 25%;">
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="../product/index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="../product/add.php">ADD Product</a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Add Category</a>
                        
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Category List</a>
                        
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Other</a>
                        
                    </li>
                  
                    

                </ul>

            </div>
        </div>
    </nav>

     <div class="container-fluid">

        <div class="row md-12">

        <div class="col-md-6">

        </div>
       <div class="col-md-6" style="margin-top: 100px;">

        <form action="create.php" method="post">
        
        <label for="title" class="form-label">Category Title</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Enter Category Title">
        <div class="col-md-6">
                        <label for="floatingTextarea2">Product Descripton</label>
                        <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px;width: 350px;" name="description">
                        </textarea>

          </div>
          <br>
          <br>

        <button type="submit">Add Category</button>

       </form>
        </div>
         
        </div>
        

     </div>
       
</body>
</html>