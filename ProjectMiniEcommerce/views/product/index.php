<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <title>Document</title>
    <style>
        a:hover {
            color: crimson;
        }
    </style>
</head>

<body>
    <script src="../../assets//js/bootstrap.bundle.min.js"></script>
    <nav class="navbar navbar-expand-lg nnavbar navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="../index.php">
                <img src="../../assets/image/rupali.jpeg" alt="" width="100" height="60" class="d-inline-block align-text-top">
                রুপালী বাজার
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 " style="margin-left: 25%;">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="add.php">Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Category/add.php">Category</a>
                    </li>
                    
                 
                </ul>

            </div>
        </div>
    </nav>


    <div class="container-fluid">

        <div class="row justify-content-md-center" style="margin-top: 50px;">
             <div class="col-md-2">
                 <a href="add.php">Add New Product</a>

             </div>
            <div class="col-md-6 text-center">
                <h4>Product List: </h4>
                <table class=" table  table-success table-striped table-hover  table-bordered border-info">
                    <thead>
                        <tr>
                            <th scope="col">SL</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Category ID</th>
                            <th scope="col">Price</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include_once '../../src/Product.php';
                        $productobj = new Product();
                        $product = $productobj->view();
                        $i = 1;

                        foreach ($product as  $value) {

                        ?>
                            <tr>
                                <th scope="row"><?= $i++ ?></th>
                                <td><?= $value['product_name'] ?></td>
                                <td><?= $value['categori_id'] ?></td>
                                <td><?= $value['price'] ?></td>
                                <td>
                                    <a href="show.php?id=<?=$value['id']?>">SHOW||</a>
                                    <a href="edit.php?id=<?=$value['id']?>">EDIT||</a>
                                    <a href="delete.php?id= <?=$value['id']?>" onclick="return confirm('Are you sure to delete this item?')">DELETE</a>
                                </td>
                            </tr>
                        <?php
                        } ?>
                    </tbody>
                </table>
            </div>

        </div>


    </div>


</body>

</html>